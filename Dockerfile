FROM node:8

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies change test for push 
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available unill
COPY package*.json ./
# RUN ["chmod", "+x", "/usr/src/app/docker-entrypoint.sh"]
RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

#EXPOSE 8080
CMD [ "npm", "start" ]