'use strict';
// sixth test
const express = require('express');
const PORT = 9090;
const HOST = '127.0.0.1';
const app = express();
app.get('/', (req, res) => {
  res.send('Hello world \n');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);